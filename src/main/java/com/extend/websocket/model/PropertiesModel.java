package com.extend.websocket.model;

public class PropertiesModel {

    private String wssEndPoint;
    private String jksPath;
    private String storePassWord;
    private String keyPassWord;
    private String storeType;
    private String sslProtocol;

    public String getWssEndPoint() {
        return wssEndPoint;
    }

    public void setWssEndPoint(String wssEndPoint) {
        this.wssEndPoint = wssEndPoint;
    }

    public String getJksPath() {
        return jksPath;
    }

    public void setJksPath(String jksPath) {
        this.jksPath = jksPath;
    }

    public String getStorePassWord() {
        return storePassWord;
    }

    public void setStorePassWord(String storePassWord) {
        this.storePassWord = storePassWord;
    }

    public String getKeyPassWord() {
        return keyPassWord;
    }

    public void setKeyPassWord(String keyPassWord) {
        this.keyPassWord = keyPassWord;
    }

    public String getStoreType() {
        return storeType;
    }

    public void setStoreType(String storeType) {
        this.storeType = storeType;
    }

    public String getSslProtocol() {
        return sslProtocol;
    }

    public void setSslProtocol(String sslProtocol) {
        this.sslProtocol = sslProtocol;
    }
}

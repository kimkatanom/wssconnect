package com.extend.websocket.client;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import java.net.URI;

public class WSClient extends WebSocketClient {

    private final Logger log = LogManager.getLogger(WSClient.class);

    public WSClient(URI serverUri) {
        super(serverUri);
    }

    @Override
    public void onOpen(ServerHandshake handshakedata) {
        log.info("Connected");
    }

    @Override
    public void onMessage(String message) {
        log.info("got: {}",message);
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        log.info("Disconnected");
    }

    @Override
    public void onError(Exception ex) {
        log.error("Error - ",ex);
    }
}

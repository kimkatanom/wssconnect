package com.extend.websocket;

import com.extend.websocket.client.WSClient;
import com.extend.websocket.config.PropertiesConfig;
import com.extend.websocket.model.PropertiesModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import java.io.File;
import java.io.FileInputStream;
import java.net.URI;
import java.nio.file.Paths;
import java.security.KeyStore;

public class WSConnect {

    private static final Logger log = LogManager.getLogger(WSConnect.class);

    public static void main(String[] args) {
        WSClient webSocketClient = null;
        try {

            PropertiesConfig propertiesConfig = new PropertiesConfig();
            PropertiesModel propertiesModel = propertiesConfig.loadProperties();

            log.info("init socket with ssl for connect ...");

            webSocketClient = new WSClient(new URI(propertiesModel.getWssEndPoint()));

            // load up the key store
            String keyStore = Paths.get(propertiesModel.getJksPath()).toString();

            KeyStore ks = KeyStore.getInstance(propertiesModel.getStoreType());
            File kf = new File(keyStore);
            ks.load(new FileInputStream(kf), propertiesModel.getStorePassWord().toCharArray());
            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(ks, propertiesModel.getKeyPassWord().toCharArray());

            SSLContext sslContext = null;
            sslContext = SSLContext.getInstance(propertiesModel.getSslProtocol());
            sslContext.init(kmf.getKeyManagers(),null, null);
            // sslContext.init( null, null, null ); // will use java's default key and trust store which is sufficient unless you deal with self-signed certificates

            SSLSocketFactory factory = sslContext
                    .getSocketFactory();// (SSLSocketFactory) SSLSocketFactory.getDefault();

            webSocketClient.setSocketFactory(factory);

            log.info("connecting with ssl via socket without message");
            //connect
            webSocketClient.connectBlocking();

            log.info("closing socket");
            //close
            webSocketClient.closeBlocking();

//            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//            while (true) {
//                String line = reader.readLine();
//                if (line.equals("close")) {
//                    webSocketClient.closeBlocking();
//                } else if (line.equals("open")) {
//                    webSocketClient.reconnect();
//                } else {
//                    webSocketClient.send(line);
//                }
//            }

        } catch (Exception e) {
            log.error("Error - ",e);
        }
    }
}

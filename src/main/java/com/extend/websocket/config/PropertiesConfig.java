package com.extend.websocket.config;

import com.extend.websocket.model.PropertiesModel;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesConfig {

    private final Logger log = LogManager.getLogger(PropertiesConfig.class);

    InputStream inputStream;

    public PropertiesModel loadProperties() throws IOException {

        log.info("PropertiesConfig - loading properties start");

        PropertiesModel properties = null;

        try {
            Properties prop = new Properties();
            String propFileName = "application.properties";

            inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }


            // get the property value and print it out
            String wssEndPoint = prop.getProperty("wss.endpoint");
            String jksPath = prop.getProperty("jks.path");
            String storePassword = prop.getProperty("store.password");
            String keyPassword = prop.getProperty("key.password");
            String storeType = prop.getProperty("store.type");
            String sslProtocol = prop.getProperty("ssl.protocol");

            log.info("wssEndpoint: {}",wssEndPoint);
            log.info("jksPath: {}",jksPath);
            log.info("storePassword: {}",storePassword);
            log.info("keyPassword: {}",keyPassword);
            log.info("storeType: {}",storeType);
            log.info("sslProtocol: {}",sslProtocol);


            boolean isEmptyProperties = isEmptyProperties(wssEndPoint,jksPath,storePassword,keyPassword,storeType,sslProtocol);
            log.info("inEmptyProperties: {}",isEmptyProperties);
            if(!isEmptyProperties) {
               log.info("init propertiesModel");
               properties = generatePropertiesModel(wssEndPoint,jksPath,storePassword,keyPassword,storeType,sslProtocol);
            }
        } catch (Exception e) {
            log.error("Error - ",e);
        } finally {
            inputStream.close();
            log.info("PropertiesConfig - loading properties end");
        }
        return properties;
    }


    private boolean isEmptyProperties(String wssEndPoint,
                                      String jksPath,
                                      String storePassword,
                                      String keyPassword,
                                      String storeType,
                                      String sslProtocol){
        boolean isEmptyProperties = true;
        boolean isEmptyWssEndPoint = StringUtils.isEmpty(wssEndPoint)?true:false;
        boolean isEmptyJksPath = StringUtils.isEmpty(jksPath)?true:false;
        boolean isEmptyStorePassword = StringUtils.isEmpty(storePassword)?true:false;
        boolean isEmptyKeyPassword = StringUtils.isEmpty(keyPassword)?true:false;
        boolean isEmptyKeyStoreType = StringUtils.isEmpty(storeType)?true:false;
        boolean isEmptySslProtocol = StringUtils.isEmpty(sslProtocol)?true:false;

        if(!isEmptyWssEndPoint || !isEmptyJksPath || !isEmptyStorePassword || !isEmptyKeyPassword || !isEmptyKeyStoreType || !isEmptySslProtocol){
            isEmptyProperties = false;
        }
        return isEmptyProperties;

    }


    private  PropertiesModel generatePropertiesModel(String wssEndPoint,
                                                     String jksPath,
                                                     String storePassword,
                                                     String keyPassword,
                                                     String storeType,
                                                     String sslProtocol){
        PropertiesModel propertiesModel = new PropertiesModel();
        propertiesModel.setWssEndPoint(wssEndPoint);
        propertiesModel.setJksPath(jksPath);
        propertiesModel.setStorePassWord(storePassword);
        propertiesModel.setKeyPassWord(keyPassword);
        propertiesModel.setStoreType(storeType);
        propertiesModel.setSslProtocol(sslProtocol);
        return propertiesModel;
    }
}
